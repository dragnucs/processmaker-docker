FROM php:7.1.32-apache-buster

LABEL maintainer="Mohamed-Touhami MAHDI <touhami@touha.me>"

COPY processmaker.conf /etc/apache2/sites-available/processmaker.conf

RUN apt-get update && apt-get install --no-install-recommends -y \
       libfreetype6-dev \
       libjpeg62-turbo-dev \
       libldap2-dev \
       libmcrypt-dev \
       libpng-dev \
       libxpm-dev \
       libwebp-dev \
       libxml2-dev \
       libzip-dev \
    && apt-get clean \
    && a2enmod headers \
    && a2enmod expires \
    && a2enmod rewrite \
    && pecl install apcu \
    && docker-php-ext-configure gd \
       --with-gd \
       --with-webp-dir \
       --with-jpeg-dir \
       --with-png-dir \
       --with-zlib-dir \
       --with-xpm-dir \
       --with-freetype-dir \
    && docker-php-ext-enable apcu \
    && docker-php-ext-install -j$(nproc) gd mcrypt mysqli pdo_mysql mbstring soap ldap zip \
    && apt-get remove -y \
       libfreetype6-dev \
       libjpeg62-turbo-dev \
       libldap2-dev \
       libmcrypt-dev \
       libpng-dev \
       libxpm-dev \
       libwebp-dev \
       libxml2-dev \
    && rm -rf /var/lib/apt/lists/* \
    && cp "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" \
    && echo "date.timezone=\"Africa/Casablanca\"" > /usr/local/etc/php/conf.d/timezone.ini \
    && echo "max_input_vars = 80000" > /usr/local/etc/php/conf.d/maxinput.ini

COPY processmaker /opt/processmaker

WORKDIR /opt/processmaker

RUN a2dissite 000-default && a2ensite processmaker.conf \
    && mkdir -p /opt/processmaker/shared/ \
                /opt/processmaker/workflow/engine/js/labels/ \
                /opt/processmaker/workflow/public_html/translations/ \
    && chown -R www-data:www-data \
       /opt/processmaker/bootstrap/cache \
       /opt/processmaker/shared/ \
       /opt/processmaker/workflow/engine/config/ \
       /opt/processmaker/workflow/engine/content/languages/ \
       /opt/processmaker/workflow/engine/js/labels/ \
       /opt/processmaker/workflow/engine/plugins/ \
       /opt/processmaker/workflow/engine/xmlform/ \
       /opt/processmaker/workflow/public_html/index.html \
       /opt/processmaker/workflow/public_html/translations/
